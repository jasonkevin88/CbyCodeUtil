package com.cby.code.global;

import android.app.Application;
import android.content.Context;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @description: GlobalParamManager
 * @author: ChenBaoYang
 * @createDate: 2022/3/4 2:19 下午
 */
public class GlobalParamManager {

    private Application application;

    private Context context;

    private Map<String, Object> paramMap = new ConcurrentHashMap<>();

    private GlobalParamManager() {}

    private static class SingletonHolder {
        private static final GlobalParamManager INSTANCE = new GlobalParamManager();
    }

    public static GlobalParamManager get() {
        return SingletonHolder.INSTANCE;
    }

    public void init(Application application) {
        this.application = application;
        this.context = application.getApplicationContext();
    }

    public Application getApplication() {
        return application;
    }

    public Context getContext() {
        return context;
    }

    public void saveParam(String key, Object value) {
        paramMap.put(key, value);
    }

    public <T> T getParam(String key) {
        return (T) paramMap.get(key);
    }
}
