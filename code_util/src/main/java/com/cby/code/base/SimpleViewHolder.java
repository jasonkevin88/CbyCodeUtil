package com.cby.code.base;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.cby.code.util.ResourcesUtils;

/**
 * @author ChenBaoyang
 * @description: SimpleViewHolder
 * @date 2021/12/9 18:04
 */
public class SimpleViewHolder {

    /** 所有控件的集合 */
    private final SparseArray<View> views;

    private View rootView;

    public SimpleViewHolder(View rootView) {
        this.rootView = rootView;
        views = new SparseArray<>();
    }

    public static SimpleViewHolder get(Context context, ViewGroup parent,
                                       int layoutId) {
        View itemView = LayoutInflater.from(context).inflate(layoutId, parent,
                false);
        SimpleViewHolder holder = new SimpleViewHolder(itemView);

        return holder;
    }

    public <T extends View> T findView(String viewResName) {
        return findView(getId(viewResName));
    }

    protected int getId(String resName) {
        return ResourcesUtils.getId(rootView.getContext(), resName);
    }

    public <T extends View> T findView(int viewResId) {
        View view = views.get(viewResId);
        if (view == null) {
            view = rootView.findViewById(viewResId);
            views.put(viewResId, view);
        }
        return (T) view;
    }

    /**
     * 设置文本
     * @param viewResName
     * @param text
     * @return
     */
    public TextView setTextView(String viewResName, CharSequence text) {
        return setTextView(getId(viewResName), text);
    }

    public TextView setTextView(int viewResId, CharSequence text) {
        TextView textView = findView(viewResId);
        textView.setText(text);
        return textView;
    }

    public <T extends View> T setViewVisible(String viewResName, boolean show) {
        View view = findView(viewResName);
        view.setVisibility(show? View.VISIBLE: View.GONE);
        return (T) view;
    }

    public <T extends View> T setViewVisible(int viewResId, boolean show) {
        View view = findView(viewResId);
        view.setVisibility(show? View.VISIBLE: View.GONE);
        return (T) view;
    }

    public <T extends View> T setViewClick(String viewResName, View.OnClickListener listener) {
        View view = findView(viewResName);
        view.setOnClickListener(listener);
        return (T) view;
    }

    public <T extends View> T setViewClick(int viewResId, View.OnClickListener listener) {
        View view = findView(viewResId);
        view.setOnClickListener(listener);
        return (T) view;
    }

    public ImageView setImageResource(int viewResId, int resId) {
        ImageView view = findView(viewResId);
        view.setImageResource(resId);
        return view;
    }

    public ImageView setImageResource(String viewResName, String resName) {
        ImageView view = findView(viewResName);
        view.setImageResource(ResourcesUtils.getDrawableId(rootView.getContext(), resName));
        return view;
    }

    public ListView setListAdapter(int viewResId, ListAdapter adapter) {
        ListView view = findView(viewResId);
        view.setAdapter(adapter);
        return view;
    }

    public ListView setListAdapter(String viewResName, ListAdapter adapter) {
        ListView view = findView(viewResName);
        view.setAdapter(adapter);
        return view;
    }

    public <T extends View> T setBackgroundResource(int viewResId, int resId) {
        View view = findView(viewResId);
        view.setBackgroundResource(resId);
        return (T) view;
    }

    public <T extends View> T setBackgroundResource(String viewResName, String resName) {
        View view = findView(viewResName);
        view.setBackgroundResource(ResourcesUtils.getDrawableId(rootView.getContext(), resName));
        return (T) view;
    }
}
