package com.cby.code.base;

import android.app.Activity;
import android.content.Context;

import java.util.List;
import java.util.Stack;

/**
 * Activity堆栈管理类
 *
 * @author 徐智伟
 * @create 2019/4/16
 */
public class ActivityManager {
  public static final String TAG = "ActivityStackManager";
  private volatile static ActivityManager sInstance = null;

  /**
   * Activity堆栈
   */
  private Stack<Activity> mActivityStack = new Stack<>();

  private ActivityManager() {

  }

  public static ActivityManager getInstance() {
    if (sInstance == null) {
      synchronized (ActivityManager.class) {
        if (sInstance == null) {
          sInstance = new ActivityManager();
        }
      }
    }
    return sInstance;
  }

  /**
   * 添加Activity到堆栈中
   */
  public void addActivity(Activity activity) {
    mActivityStack.add(activity);
  }

  /**
   * 移除当前Activity
   */
  public void removeActivity(Activity activity) {
    if (activity != null && mActivityStack.contains(activity)) {
      mActivityStack.remove(activity);
    }
  }

  /**
   * 获取当前Activity(堆栈中最后一个Activity,位于栈顶)
   */
  public Activity getCurrentActivity() {
    if(mActivityStack != null && !mActivityStack.isEmpty()) {
      return mActivityStack.lastElement();
    }
    return null;
  }

  /**
   * 获取指定类名的Activity
   */
  public Activity getActivity(Class<?> cls) {
    if (mActivityStack != null) {
      for (Activity activity : mActivityStack) {
        if (activity.getClass().equals(cls)) {
          return activity;
        }
      }
    }
    return null;
  }

  /**
   * 结束当前Activity(栈顶)
   */
  public void finishCurrentActivity() {
    Activity activity = mActivityStack.lastElement();
    activity.finish();
  }


  /**
   * 结束当前Activity的前一个Activity
   */
  public void finishPreActivity() {

    if (mActivityStack != null && mActivityStack.size() > 1) {
        Activity preActivity = mActivityStack.get(mActivityStack.size() - 2);
        if (preActivity != null && !preActivity.isFinishing()) {
          preActivity.finish();
        }
    }

  }


  /**
   * 结束指定的Activity
   */
  public void finishActivity(Activity activity) {
    if (activity != null && mActivityStack.contains(activity)) {
      mActivityStack.remove(activity);
      activity.finish();
    }
  }

  /**
   * 清理所有的Activity
   */
  public void finishAllActivity() {
    for (Activity activity : mActivityStack) {
      if (!activity.isFinishing()) {
        activity.finish();
      }
    }
    mActivityStack.clear();
  }

  /**
   * 移除自己以外的Activity
   */
  public void removeAboveActivities(Class activityClass) {
    Activity targetActivity = null;
    try {
      while (mActivityStack.size() > 0) {
        Activity activity = mActivityStack.pop();
        if(!activity.getClass().equals(activityClass)) {
          activity.finish();
        }else {
          targetActivity = activity;
        }
      }
      if(targetActivity != null) {
        mActivityStack.add(targetActivity);
      }
    } catch (Exception e) {

    }

  }

  public int getAliveActivities() {
    return mActivityStack.size();
  }

  /**
   * 退出app
   */
  public void exitApp(Activity activity) {
    try {

      finishAllActivity();
      killAllProcess(activity);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void killAllProcess(Context context){
    android.app.ActivityManager mActivityManager = (android.app.ActivityManager) context.getSystemService(
        Context.ACTIVITY_SERVICE);
    List<android.app.ActivityManager.RunningAppProcessInfo> mList = mActivityManager.getRunningAppProcesses();
    for (android.app.ActivityManager.RunningAppProcessInfo runningAppProcessInfo : mList) {
      if (runningAppProcessInfo.pid != android.os.Process.myPid()) {
        android.os.Process.killProcess(runningAppProcessInfo.pid);
      }
    }

    //干掉主进程
    //android.os.Process.killProcess(android.os.Process.myPid());

  }

}
