package com.cby.code.util;

import android.os.Build;
import android.util.Xml;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;

/**
 * @author ChenBaoyang
 * @description: WebUtils
 * @date 6/9/21 17:18
 */
public class WebUtils {
    public static final String APP_CACAHE_DIRNAME = "/webcache";

    public static WebSettings setWebSettings(WebView webView) {

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD_MR1
                && Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            webView.removeJavascriptInterface("searchBoxJavaBridge_");
            webView.removeJavascriptInterface("accessibility");
            webView.removeJavascriptInterface("accessibilityTraversal");
        }

        //每次进入都要清理缓存
//        webView.clearHistory();
//        webView.clearCache(true);
//        webView.clearFormData();

        //设置scrollBar隐藏
        webView.setHorizontalScrollBarEnabled(false);
        webView.setVerticalScrollBarEnabled(false);
        webView.setScrollbarFadingEnabled(false);

        WebSettings webSettings = webView.getSettings();
        //setting配置
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setSavePassword(false);
        //支持多窗口
        webSettings.setSupportMultipleWindows(true);
        webSettings.setAppCacheMaxSize(1024 * 1023 * 25);
        webSettings.setTextZoom(100);

        if (Build.VERSION.SDK_INT < 18) {
            webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        }

        //允许自动播放音视频音乐
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            webSettings.setMediaPlaybackRequiresUserGesture(false);
        }

        /**
         * 允许第三方cookie
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true);
            //允许http/https混合内容
            webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

        webSettings.setJavaScriptCanOpenWindowsAutomatically(false);
        webSettings.setSupportMultipleWindows(false);

        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);

        // 是否允许WebView使用File协议，移动版的Chrome默认禁止加载file协议的文件；
        webSettings.setAllowFileAccess(false);
        if (Build.VERSION.SDK_INT >= 16) {
            webSettings.setAllowFileAccessFromFileURLs(false);
            webSettings.setAllowUniversalAccessFromFileURLs(false);
        }
        //设置缓存
        webSettings.setDatabaseEnabled(true);
        webSettings.setAppCacheEnabled(true);
        String cacheDirPath = webView.getContext()
                .getFilesDir().getAbsolutePath() + APP_CACAHE_DIRNAME;

        webSettings.setAppCachePath(cacheDirPath);
        //屏幕自适应
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setSupportZoom(false);
        webSettings.setBuiltInZoomControls(false);
        webSettings.setNeedInitialFocus(false);
        webSettings.setDefaultTextEncodingName(String.valueOf(Xml.Encoding.UTF_8));

        return webSettings;
    }

    public static void clearDataAndCache(WebView webView) {
        if(webView == null) {
            return;
        }
        //清除缓存
        webView.clearCache(true);
        webView.clearFormData();
        webView.clearHistory();
    }
}
