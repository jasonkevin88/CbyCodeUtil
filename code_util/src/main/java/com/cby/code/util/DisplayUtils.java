package com.cby.code.util;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;

/**
 * @description: DisplayUtils
 * @author: ChenBaoYang
 * @createDate: 2023/2/17 2:35 PM
 */
public class DisplayUtils {

    public void initFontScale(Resources resources){
        Configuration configuration = resources.getConfiguration();
        //0.85:小号  1:标准  1.25:大号  1.4:巨无霸
        configuration.fontScale = 1.0f;
        configuration.setToDefaults();
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
    }

    /**
     * 设置App不跟随系统[显示]大小变化
     * @See https://www.jianshu.com/p/bb3c23a3cf1e
     * @param context
     */
    public static void setDefaultDisplay(Context context) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            Resources res = context.getResources();
            Configuration configuration = res.getConfiguration();
            if (res.getDisplayMetrics().densityDpi != DisplayMetrics.DENSITY_DEVICE_STABLE) {
                configuration.densityDpi = DisplayMetrics.DENSITY_DEVICE_STABLE;
                res.updateConfiguration(configuration, res.getDisplayMetrics());
            }
        }
    }
}
