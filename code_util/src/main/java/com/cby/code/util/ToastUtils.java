package com.cby.code.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.Toast;



import java.lang.reflect.Field;

/**
 * @author ChenBaoyang
 * @description: ToastUtils
 * @date 2021/11/6 11:10
 */
public class ToastUtils {

    public static void showToast(Context context, String text) {
        Toast toast = Toast.makeText(context.getApplicationContext(), text, Toast.LENGTH_SHORT);
        fixBadTokenException(toast);
        toast.show();
    }

    public static void showToast(Context context, int resId) {
        Toast toast = Toast.makeText(context.getApplicationContext(), resId, Toast.LENGTH_SHORT);
        fixBadTokenException(toast);
        toast.show();
    }

    public static void showLongToast(Context context, String text) {
        Toast toast = Toast.makeText(context.getApplicationContext(), text, Toast.LENGTH_LONG);
        fixBadTokenException(toast);
        toast.show();
    }

    public static void showLongToast(Context context, int resId) {
        Toast toast = Toast.makeText(context.getApplicationContext(), resId, Toast.LENGTH_LONG);
        fixBadTokenException(toast);
        toast.show();
    }

    public static void showShort(Context context, int resId, int top) {
        Toast toast = Toast.makeText(context.getApplicationContext(),
                resId, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP, 0, top);
        fixBadTokenException(toast);
        toast.show();
    }

    public static void showShort(Context context, String message, int top) {
        Toast toast = Toast.makeText(context.getApplicationContext(),
                message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP, 0, top);
        fixBadTokenException(toast);
        toast.show();
    }


    /**
     * Android N Toast出现badToken问题
     * @param origin
     */
    private static void fixBadTokenException(Toast origin) {
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N_MR1) {
                try {
                    @SuppressLint("DiscouragedPrivateApi")
                    Field tnField = Toast.class.getDeclaredField("mTN");
                    tnField.setAccessible(true);
                    Object tn = tnField.get(origin);

                    Field handlerField = tnField.getType().getDeclaredField("mHandler");
                    handlerField.setAccessible(true);
                    Handler handler = (Handler) handlerField.get(tn);

                    @SuppressLint("RestrictedApi")
                    Looper looper = Looper.myLooper();
                    if (looper == null) {
                        throw new NullPointerException("Can't toast on a thread that has not called Looper.prepare()");
                    }
                    handlerField.set(tn, new Handler(looper) {
                        @Override
                        public void handleMessage(Message msg) {
                            try {
                                handler.handleMessage(msg);
                            } catch (WindowManager.BadTokenException e) {
                            }
                        }
                    });
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                }
            }
    }
}
