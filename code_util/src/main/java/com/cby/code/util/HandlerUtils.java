package com.cby.code.util;

import android.os.Handler;
import android.os.Looper;

/**
 * Description:HandlerUtils
 *
 * @author chenbaoyang
 * @create 2020/8/3 16:49
 */
public class HandlerUtils {

  private static Handler sMainHandler = new Handler(Looper.getMainLooper());

  /**
   * 在主线程中执行
   * @param task
   */
  public static void runOnMainThread(Runnable task) {
    sMainHandler.post(task);
  }


  /**
   * 是否是主线程
   * @return
   */
  public static boolean isMainThread(){
    return Thread.currentThread() == Looper.getMainLooper().getThread();
  }
}
