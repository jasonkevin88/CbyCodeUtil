package com.cby.code.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * @description: DateUtils
 * @author: ChenBaoYang
 * @createDate: 2022/4/13 2:46 下午
 */
public class DateUtils {

    /** 时间格式为 HH:mm*/
    private static final SimpleDateFormat DATE_FORMAT_H_M = new SimpleDateFormat("HH:mm");
    /** 时间格式为 HH:mm:ss*/
    private static final SimpleDateFormat DATE_FORMAT_H_M_S = new SimpleDateFormat("HH:mm:ss");
    /** 时间格式为 yyyy-MM-dd*/
    private static final SimpleDateFormat DATE_FORMAT_Y_M_D = new SimpleDateFormat("yyyy-MM-dd");
    /** 时间格式为 yyyy-MM-dd HH:mm*/
    private static final SimpleDateFormat DATE_FORMAT_Y_M_D_H_M = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    /** 时间格式为 yyyy-MM-dd HH:mm:ss*/
    private static final SimpleDateFormat DATE_FORMAT_Y_M_D_H_M_S = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");




    /**
     * 获取当前时间，格式为HH:mm
     */
    public static String getNowTimeFormatHM() {
        // 获取当前时间
        Date date = new Date(System.currentTimeMillis());
        return DATE_FORMAT_H_M.format(date);
    }

    /**
     * 获取当前时间，格式为HH:mm:ss
     */
    public static String getNowTimeFormatHMS() {
        // 获取当前时间
        Date date = new Date(System.currentTimeMillis());
        return DATE_FORMAT_H_M_S.format(date);
    }

    /**
     * 获取当前时间，格式为yyyy-MM-dd
     */
    public static String getNowTimeFormatYMD() {
        // 获取当前时间
        Date date = new Date(System.currentTimeMillis());
        return DATE_FORMAT_Y_M_D.format(date);
    }

    /**
     * 获取当前时间，格式为yyyy-MM-dd HH:mm
     */
    public static String getNowTimeFormatYMDHM() {
        // 获取当前时间
        Date date = new Date(System.currentTimeMillis());
        return DATE_FORMAT_Y_M_D_H_M.format(date);
    }


    /**
     * 获取当前时间，格式为yyyy-MM-dd HH:mm:ss
     */
    public static String getNowTimeFormatYMDHMS() {
        // 获取当前时间
        Date date = new Date(System.currentTimeMillis());
        return DATE_FORMAT_Y_M_D_H_M_S.format(date);
    }

    /**
     * 获取时间格式
     * @param time
     * @param format
     * @return
     */
    public static String getTime(long time, SimpleDateFormat format) {
        return getTime(new Date(time), format);
    }

    /**
     * 获取时间格式
     * @param date
     * @param format
     * @return
     */
    public static String getTime(Date date, SimpleDateFormat format) {
        return format.format(date);
    }

    /**
     * 获取现在的小时
     * @return
     */
    public static int getNowHour() {
        return Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
    }

    /**
     * 将'08:00:00'的时间转成int的小时
     *
     * @param time
     * @return
     */
    public int parseHour(String time) {
        Calendar calendar = Calendar.getInstance();
        Date date;
        try {
            date = DATE_FORMAT_H_M_S.parse(time);
            calendar.setTime(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return calendar.get(Calendar.HOUR_OF_DAY);
    }

    /**
     * 是否是周末
     *
     * @return
     */
    public static boolean isWeekend() {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
                || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            return true;
        } else {
            return false;
        }
    }
}
