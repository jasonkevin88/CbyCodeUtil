package com.cby.code.helper;

import android.content.Context;
import android.content.pm.ActivityInfo;

/**
 * @description: OrientationSpinnerHelper
 * @author: ChenBaoYang
 * @createDate: 2022/3/21 11:04 上午
 */
public class OrientationSpinnerHelper extends SpinnerHelper<String, Integer> {

    public static final Integer[] ORIENTATION = new Integer[]{
            // 1，竖屏
            ActivityInfo.SCREEN_ORIENTATION_PORTRAIT,
            // 0，横屏
            ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE

    };

    public static final String[] ORIENTATION_NAMES = new String[]{
            "竖屏",
            "横屏"
    };

    public OrientationSpinnerHelper(Context context) {
        super(context, "orientation");
        setKeys(ORIENTATION_NAMES);
        setValues(ORIENTATION);
    }
}
