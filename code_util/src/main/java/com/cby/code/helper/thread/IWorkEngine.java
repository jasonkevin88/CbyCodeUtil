package com.cby.code.helper.thread;

/**
 * @author chenbaoyang
 */
public abstract class IWorkEngine {

    /** 要处理的任务 */
    protected WorkTask workTask;

    /** 任务执行调度者 */
    protected Dispatcher workDispatcher;

    /** 任务结束调度者 */
    protected Dispatcher endDispatcher;

    public IWorkEngine(WorkTask workTask, Dispatcher workDispatcher, Dispatcher endDispatcher) {
        this.workTask = workTask;
        this.workDispatcher = workDispatcher;
        this.endDispatcher = endDispatcher;
    }

    public void start() {
        runWork();
    }

    protected abstract void runWork();


}
