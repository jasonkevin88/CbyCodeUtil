package com.cby.code.helper.thread;

/**
 * 调度者：主线程，当前线程，新线程
 * @author chenbaoyang
 */
public class Dispatcher {

    /** 主线程 */
    public static final int MAIN_THREAD = 1;
    /** 当前线程 */
    public static final int CURRENT_THREAD = 2;
    /** 新线程 */
    public static final int NEW_THREAD = 3;

    private int dispatcherThread = CURRENT_THREAD;

    public Dispatcher handleOnMainThread() {
        dispatcherThread = MAIN_THREAD;
        return this;
    }

    public Dispatcher handleOnCurrentThread() {
        dispatcherThread = CURRENT_THREAD;
        return this;
    }

    public Dispatcher handOnNewThread() {
        dispatcherThread = NEW_THREAD;
        return this;
    }

    public int getDispatcherThread() {
        return dispatcherThread;
    }

    public static Dispatcher getDefault() {
        return new Dispatcher().handleOnCurrentThread();
    }

    public static Dispatcher getMainThread() {
        return new Dispatcher().handleOnMainThread();
    }

    public static Dispatcher getNewThread() {
        return new Dispatcher().handOnNewThread();
    }

}
