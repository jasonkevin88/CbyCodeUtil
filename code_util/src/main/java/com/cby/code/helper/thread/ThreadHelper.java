package com.cby.code.helper.thread;

import java.util.HashMap;
import java.util.Map;

/**
 * @author chenbaoyang
 */
public class ThreadHelper {

    private Dispatcher workDispatcher = Dispatcher.getDefault();

    private Dispatcher endDispatcher = Dispatcher.getDefault();

    private WorkTask workTask;

    /** 用来存档任务和对应的任务引擎 */
    private Map<WorkTask, IWorkEngine> engineMap = new HashMap<>();

    public ThreadHelper workIn(Dispatcher workDispatcher) {
        this.workDispatcher = workDispatcher;
        return this;
    }

    public ThreadHelper endIn(Dispatcher endDispatcher) {
        this.endDispatcher = endDispatcher;
        return this;
    }

    public void handleWork(WorkTask workTask) {
        this.workTask = workTask;
//        IWorkEngine engine = engineMap.get(workTask);
//        if(engine != null) {
//            throw new RuntimeException("workTask already create the work engine.");
//        }
        IWorkEngine engine = new RealWorkEngine(workTask, workDispatcher, endDispatcher);
        //engineMap.put(workTask, engine);
        engine.start();
    }

//    private SingleThread thread = null;

//    class SingleThread extends Thread {
//
//        private static final int ON_WORK = 0x001;
//        private static final int ON_FINISH = 0x002;
//
//        private WorkTask workTask;
//        private boolean workInMainThread;
//        private boolean endInMainThread;
//
//        private Handler handler;
//
//        public SingleThread(WorkTask workTask, boolean workInMainThread, boolean endInMainThread) {
//            this.workTask = workTask;
//            this.workInMainThread = workInMainThread;
//            this.endInMainThread = endInMainThread;
//        }
//
//        @Override
//        public void run() {
//            //创建与当前线程相关的Looper
//            Looper.prepare();
//
//            if (workInMainThread) {
//                handler = new InternalHandler(Looper.getMainLooper());
//            }else{
//                handler = new InternalHandler(Looper.myLooper());
//            } //获取当前线程的Looper对象
//            Log.d("HandlerThreadActivity", "Thread3------" + currentThread());
//
//            //执行work接口的操作
//            sendMessage(workTask);
//            Looper.loop(); //调用此方法，消息才会循环处理
//        }
//
//        public void quit() {
//            Looper looper = Looper.myLooper();
//            if(looper != null) {
//                looper.quit();
//            }
//        }
//
//        private void sendMessage(WorkTask work) {
//            Message message = handler.obtainMessage(ON_WORK, work);
//            message.sendToTarget();
//        }
//
//        class InternalHandler extends Handler {
//
//            public InternalHandler(Looper looper) {
//                super(looper);
//            }
//
//            @Override
//            public void handleMessage(Message msg) {
//                WorkTask work  = (WorkTask) msg.obj;
//                switch (msg.what) {
//                    case ON_WORK:
//                        onWork();
//                        break;
//                    case ON_FINISH:
//                        work.onFinish();
//                        break;
//                }
//            }
//        }
//
//        private void onWork() {
//            workTask.doWork();
//            boolean isOnMain = handler.getLooper() == Looper.getMainLooper();
//            if(endInMainThread) {
//                if(!isOnMain) {
//                    handler = new InternalHandler(Looper.getMainLooper());
//                }
//                Message message = handler.obtainMessage(ON_FINISH, workTask);
//                message.sendToTarget();
//            }else{
//                workTask.onFinish();
//            }
//
//        }
//    }
//
//
//    public void handWork(WorkTask workTask, boolean isMainThread, boolean endMainThread) {
//        thread = new SingleThread(workTask, isMainThread, endMainThread);
//        thread.start();
//    }


//    public void quitWork() {
//        thread.quit();
//    }
}
