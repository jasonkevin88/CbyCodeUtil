package com.cby.code.helper.thread;

/**
 * @author chenbaoyang
 */
public interface WorkTask {
    void doWork();
    void onFinish();
}
