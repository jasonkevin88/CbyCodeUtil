package com.cby.code.helper.thread;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;


public class RealWorkEngine extends IWorkEngine{

    private static final String TAG = "RealWorkEngine";

    private static final int ON_WORK = 0x001;
    private static final int ON_FINISH = 0x002;

    /** 记录最初的线程 */
    private Looper originLooper;

    public RealWorkEngine(WorkTask workTask, Dispatcher workDispatcher, Dispatcher endDispatcher) {
        super(workTask, workDispatcher, endDispatcher);
    }

    @Override
    protected void runWork() {
        // 记录最初的线程
        originLooper = Looper.myLooper();
        checkDispatcherWorkThread();
    }

    protected void checkDispatcherWorkThread() {

        switch (workDispatcher.getDispatcherThread()) {
            case Dispatcher.MAIN_THREAD:
                checkMainThreadWork();
                break;

            case Dispatcher.CURRENT_THREAD:
                checkCurrentThreadWork();
                break;

            case Dispatcher.NEW_THREAD:
                checkNewThreadWork();
                break;
        }
    }

    protected void checkDispatcherEndThread() {
        switch (endDispatcher.getDispatcherThread()) {
            case Dispatcher.MAIN_THREAD:
                checkMainThreadEnd();
                break;

            case Dispatcher.CURRENT_THREAD:
                checkCurrentThreadEnd();
                break;

            case Dispatcher.NEW_THREAD:
                checkNewThreadEnd();
                break;
        }
    }

    /**
     * 执行任务，检测是否在主线程，不在的话，切换至主线程执行任务
     */
    protected void checkMainThreadWork() {
        if(Looper.myLooper() == Looper.getMainLooper() && workTask != null) {
            //当前线程在主线程
            workTask.doWork();
            checkDispatcherEndThread();
        }else {
            InternalHandler handler = new InternalHandler(Looper.getMainLooper());
            Message message = handler.obtainMessage(ON_WORK, workTask);
            message.sendToTarget();
        }
    }

    /**
     * 执行任务，检测是否在用户最初线程，不在的话，切换至用户最初线程执行任务
     */
    protected void checkCurrentThreadWork() {
        if(originLooper == Looper.myLooper() && workTask != null) {
            workTask.doWork();
            checkDispatcherEndThread();
        }else {
            InternalHandler handler = new InternalHandler(originLooper);
            Message message = handler.obtainMessage(ON_WORK, workTask);
            message.sendToTarget();
        }
    }

    /**
     * 直接创建一个新线程执行任务
     */
    protected void checkNewThreadWork() {
        SingleWorkThread workThread = new SingleWorkThread(workTask, new SingleWorkThread.CallBack() {
            @Override
            public void onEnd() {
                checkDispatcherEndThread();
            }
        });
        workThread.start();
    }

    /**
     * 执行任务，检测是否在主线程，不在的话，切换至主线程结束任务
     */
    protected void checkMainThreadEnd() {
        if(Looper.myLooper() == Looper.getMainLooper() && workTask != null) {
            //当前线程在主线程
            workTask.onFinish();
        }else {
            InternalHandler handler = new InternalHandler(Looper.getMainLooper());
            Message message = handler.obtainMessage(ON_FINISH, workTask);
            message.sendToTarget();
        }
    }

    /**
     * 执行任务，检测是否在用户最初线程，不在的话，切换至用户最初线程执行任务
     */
    protected void checkCurrentThreadEnd() {
        if(originLooper == Looper.myLooper() && workTask != null) {
            workTask.onFinish();
        }else {
            InternalHandler handler = new InternalHandler(originLooper);
            Message message = handler.obtainMessage(ON_FINISH, workTask);
            message.sendToTarget();
        }
    }

    /**
     * 直接创建一个新线程执行任务
     */
    protected void checkNewThreadEnd() {
        new Thread() {
            @Override
            public void run() {
                if(workTask != null) {
                    workTask.onFinish();
                }
            }
        }.start();
    }

    class InternalHandler extends Handler{

        public InternalHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            Log.e(TAG, "InternalHandler 线程ID = "+Thread.currentThread().getId());
            WorkTask work  = (WorkTask) msg.obj;
            switch (msg.what) {
                case ON_WORK:
                    work.doWork();
                    checkDispatcherEndThread();
                    break;
                case ON_FINISH:
                    work.onFinish();
                    break;
            }
        }
    }
}
