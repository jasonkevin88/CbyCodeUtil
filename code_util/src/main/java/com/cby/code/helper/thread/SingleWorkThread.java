package com.cby.code.helper.thread;

public class SingleWorkThread extends Thread {

    private WorkTask workTask;
    private CallBack callBack;

    public SingleWorkThread(WorkTask workTask, CallBack callBack) {
        this.workTask = workTask;
        this.callBack = callBack;
    }

    @Override
    public void run() {
        if(workTask != null) {
            workTask.doWork();
        }
        if(callBack != null) {
            callBack.onEnd();
        }
    }

    interface CallBack {
        void onEnd();
    }
}
