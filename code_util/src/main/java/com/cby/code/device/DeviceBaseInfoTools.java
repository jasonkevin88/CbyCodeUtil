package com.cby.code.device;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;

/**
 * @description: 设备基本信息
 * @author: ChenBaoYang
 * @createDate: 2022/3/3 4:37 下午
 */
public final class DeviceBaseInfoTools {

    private DeviceBaseInfoTools() {}

    private static class SingletonHolder {
        private static final DeviceBaseInfoTools INSTANCE = new DeviceBaseInfoTools();
    }

    public static DeviceBaseInfoTools get() {
        return SingletonHolder.INSTANCE;
    }

    /**
     * 设备产品
     */
    private String getProduct() {
        return android.os.Build.PRODUCT;
    }

    /**
     * 支持的cpu的abi
     */
    private String getSupportAbi() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            String supportAbi = "";
            for (int i = 0; i < Build.SUPPORTED_ABIS.length; i++) {
                supportAbi += Build.SUPPORTED_ABIS[i];
            }
            return supportAbi;
        }else {
            return Build.CPU_ABI;
        }
    }

    /**
     * 支持的32位cpu的abi
     * @return
     */
    private String getSupport32Abi() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            String supportAbi = "";
            for (int i = 0; i < Build.SUPPORTED_32_BIT_ABIS.length; i++) {
                supportAbi += Build.SUPPORTED_32_BIT_ABIS[i];
            }
            return supportAbi;
        }else {
            return "unknown";
        }
    }

    /**
     * 支持的64位cpu的abi
     * @return
     */
    private String getSupport64Abi() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            String supportAbi = "";
            for (int i = 0; i < Build.SUPPORTED_64_BIT_ABIS.length; i++) {
                supportAbi += Build.SUPPORTED_64_BIT_ABIS[i];
            }
            return supportAbi;
        }else {
            return Build.CPU_ABI;
        }
    }

    /**
     * 标签
     * @return
     */
    private String getTags() {
        return Build.TAGS;
    }

    /**
     * 基带版本
     * @return
     */
    private int getBaseVersionCode() {
        return Build.VERSION_CODES.BASE;
    }

    /**
     * 型号
     * @return
     */
    private String getModel() {
        return Build.MODEL;
    }

    /**
     * 设备的对于的sdk版本
     * @return
     */
    private int getSDK(){
        return Build.VERSION.SDK_INT;
    }

    /**
     * Android系统版本
     * @return
     */
    private String getOsVersion() {
        return Build.VERSION.RELEASE;
    }

    /**
     * 获取驱动
     * @return
     */
    private String getDevice(){
        return Build.DEVICE;
    }

    /**
     * 显示
     * @return
     */
    private String getDisplay() {
        return Build.DISPLAY;
    }

    /**
     * 品牌
     * @return
     */
    private String getBrand() {
        return Build.BRAND;
    }

    /**
     * 主板
     * @return
     */
    private String getBoard() {
        return Build.BOARD;
    }

    /**
     * 设备标识
     * @return
     */
    private String getFingerPrint() {
        return Build.FINGERPRINT;
    }

    /**
     * 版本号
     * @return
     */
    private String getId() {
        return Build.ID;
    }

    /**
     * 制造商
     * @return
     */
    private String getManufacturer() {
        return Build.MANUFACTURER;
    }

    /**
     * 设备用户
     * @return
     */
    private String getUser() {
        return Build.USER;
    }

    /**
     * 硬件
     * @return
     */
    private String getHardWare() {
        return Build.HARDWARE;
    }

    /**
     * 主机地址
     * @return
     */
    private String getHost() {
        return Build.HOST;
    }

    /**
     * 未知信息
     * @return
     */
    private String getUnknown() {
        return Build.UNKNOWN;
    }

    /**
     * 版本类型
     * @return
     */
    private String getType() {
        return Build.TYPE;
    }

    /**
     * 出厂时间
     * @return
     */
    private String getTime() {
        return String.valueOf(android.os.Build.TIME);
    }

    /**
     * 音频版本
     * @return
     */
    private String getRadioVersion() {
        return Build.getRadioVersion();
    }

    /**
     * 序列号
     * @return
     */
    private String getSerial() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return Build.getSerial();
        }
        return Build.SERIAL;
    }

    /**
     * Return the android id of device.
     */
    @SuppressLint("HardwareIds")
    public static String getAndroidID(Context context) {
        String id = Settings.Secure.getString(
                context.getContentResolver(),
                Settings.Secure.ANDROID_ID
        );
        return id == null ? "" : id;
    }


}
