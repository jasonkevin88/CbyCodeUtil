package com.cby.code.encrypt;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

/**
 * Description:Md5Utils
 *
 * @author 陈宝阳
 * @create 2020/8/3 16: 53
 */
public class Md5Utils {

  /**
   * md5加密字符串，默认是小写的字符
   * @param originText
   * @return
   */
  public static String md5(String originText) {
    if (originText == null || originText.length() == 0) {
      return "";
    }
    MessageDigest md5 = null;
    try {
      md5 = MessageDigest.getInstance("MD5");
      byte[] bytes = md5.digest(originText.getBytes());
      StringBuilder result = new StringBuilder();
      for (byte b : bytes) {
        String temp = Integer.toHexString(b & 0xff);
        if (temp.length() == 1) {
          temp = "0" + temp;
        }
        result.append(temp);
      }
      return result.toString();
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    return "";
  }

  /**
   * md5加密字符串，默认是小写的字符
   * @param originText
   * @return
   */
  public static String md5UpperCase(String originText) {
    return md5(originText).toUpperCase(Locale.ROOT);
  }

  /**
   * md5加密字符串，默认是小写的字符
   * @param originText
   * @return
   */
  @Deprecated
  public static String md5Way1(String originText) {
    char[] hexDigits = { // 用来将字节转换成 16 进制表示的字符
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd',
            'e', 'f'};
    try {
      byte[] strTemp = originText.getBytes();
      MessageDigest mdTemp = MessageDigest.getInstance("MD5");
      mdTemp.update(strTemp);
      // MD5 的计算结果是一个 128 位的长整数，
      byte tmp[] = mdTemp.digest();
      // 用字节表示就是 16 个字节
      // 每个字节用 16 进制表示的话，使用两个字符，
      char strs[] = new char[16 * 2];
      // 所以表示成 16 进制需要 32 个字符
      // 表示转换结果中对应的字符位置
      int k = 0;
      // 从第一个字节开始，对 MD5 的每一个字节
      for (int i = 0; i < 16; i++) {
        // 转换成 16 进制字符的转换
        // 取第 i 个字节
        byte byte0 = tmp[i];
        // 取字节中高 4 位的数字转换,
        strs[k++] = hexDigits[byte0 >>> 4 & 0xf];
        // >>> 为逻辑右移，将符号位一起右移
        // 取字节中低 4 位的数字转换
        strs[k++] = hexDigits[byte0 & 0xf];
      }
      // 换后的结果转换为字符串
      return new String(strs).toUpperCase(Locale.ROOT);
    } catch (Exception e) {
      return null;
    }
  }

  /**
   * md5加密字符串，默认是小写的字符
   * @param originText
   * @return
   */
  @Deprecated
  public static String md5Way2(String originText) {
    try {
      MessageDigest md = MessageDigest.getInstance("MD5");
      byte[] md5Bytes = md.digest(originText.getBytes());
      String md5Str = bytes2Hex(md5Bytes);
      return md5Str;
    }catch (Exception e) {
      return null;
    }
  }

  /**
   * 2进制转16进制
   * @param bytes
   * @return
   */
  private static String bytes2Hex(byte[] bytes) {
    StringBuffer result = new StringBuffer();
    int temp;
    try {
      for (int i = 0; i < bytes.length; i++) {
        temp = bytes[i];
        if(temp < 0) {
          temp += 256;
        }
        if (temp < 16) {
          result.append("0");
        }
        result.append(Integer.toHexString(temp));
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return result.toString();
  }
}
