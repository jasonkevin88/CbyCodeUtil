package com.cby.code.encrypt;

/**
 * Description:Base64Utils
 *
 * @author 陈宝阳
 * @create 2020/8/3 16:48
 */
public class Base64Utils {

  public static String decode(String data) {
    if (null == data) {
      return null;
    }
    return new String(Base64.decode(data.getBytes(), Base64.NO_WRAP));
  }

  public static byte[] decodeToByte(String data) {
    if (null == data) {
      return null;
    }
    return Base64.decode(data.getBytes(), Base64.NO_WRAP);
  }

  public static String encode(String data) {
    if (null == data) {
      return null;
    }
    return Base64.encodeToString(data.getBytes(), Base64.NO_WRAP);
  }

  public static String encode(byte[] data) {
    if (null == data) {
      return null;
    }
    return Base64.encodeToString(data, Base64.NO_WRAP);
  }

}
