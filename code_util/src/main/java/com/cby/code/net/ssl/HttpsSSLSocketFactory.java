package com.cby.code.net.ssl;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

/**
 * Description:HttpsSSLSocketFactory
 *
 * @author 陈宝阳
 * @create 2020/8/3 14: 46
 */
public class HttpsSSLSocketFactory {

  public static SSLSocketFactory factory() {
    SSLContext sslContext = null;
    try {
      sslContext = SSLContext.getInstance("SSL");
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    TrustManager[] tm = { new HttpsX509TrustManager() };
    try {
      sslContext.init(null, tm, new java.security.SecureRandom());
    } catch (KeyManagementException e) {
      e.printStackTrace();
    }
    return sslContext.getSocketFactory();
  }
}
