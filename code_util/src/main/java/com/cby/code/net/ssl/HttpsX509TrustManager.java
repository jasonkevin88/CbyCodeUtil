package com.cby.code.net.ssl;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.X509TrustManager;

/**
 * Description:HttpsX509TrustManager
 *
 * @author 陈宝阳
 * @create 2020/8/3 14: 44
 */
public class HttpsX509TrustManager  implements X509TrustManager {
  @Override
  public void checkClientTrusted(X509Certificate[] chain, String authType) throws
      CertificateException {

  }

  @Override
  public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

  }

  @Override
  public X509Certificate[] getAcceptedIssuers() {
    return new X509Certificate[0];
  }
}

