package com.cby.code.net.engine;

import com.cby.code.net.Request;
import com.cby.code.net.Response;
import com.cby.code.net.task.BaseTask;
import com.cby.code.net.task.HttpUrlStringTask;

/**
 * @description: ExecuteEngine
 * @author: ChenBaoYang
 * @createDate: 2022/2/15 2:19 下午
 */
public class DefaultEngine extends BaseEngine {

    private BaseTask task;

    private Request request;

    public DefaultEngine(Request request) {
        this.task = new HttpUrlStringTask();
        this.request = request;
    }

    public DefaultEngine(BaseTask task, Request request) {
        this.task = task;
        this.request = request;
    }

    @Override
    public void doWork() {
        execute(new Runnable() {
            @Override
            public void run() {
                Response response = task.performRequest(request);
                handleCallBack(response);
            }
        });

    }
}
