package com.cby.code.net.listener;


import com.cby.code.net.Response;

/**
 * @description: ResponseListener
 * @author: ChenBaoYang
 * @createDate: 2022/2/15 3:11 下午
 */
public interface ResponseListener {

    /**
     * 响应回调
     * @param response 响应数据{@linkplain Response}
     */
    void onResponse(Response response);
}
