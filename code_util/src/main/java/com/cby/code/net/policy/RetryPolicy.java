package com.cby.code.net.policy;

/**
 * @description: 重试策略
 * @author: ChenBaoYang
 * @createDate: 2022/1/18 5:42 下午
 */
public interface RetryPolicy {


    /**
     * 返回当前超时（用于日志记录）
     */
    int getCurrentTimeout();

    /**
     * 返回当前重试计数（用于日志记录）
     */
    int getCurrentRetryCount();

    /**
     * 通过对超时应用回退来准备下次重试.
     * @param error 异常
     */
    void retry(Exception error);

}
