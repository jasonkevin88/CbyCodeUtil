package com.cby.code.net;

/**
 * @description: Response
 * @author: ChenBaoYang
 * @createDate: 2022/2/14 5:34 下午
 */
public class Response {

    private String data;

    private int code;

    private String errorMsg;

    public Response(String data, int code, String errorMsg) {
        this.data = data;
        this.code = code;
        this.errorMsg = errorMsg;
    }

    public String getData() {
        return data;
    }

    public int getCode() {
        return code;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    @Override
    public String toString() {
        return "Response{" +
                "data='" + data + '\'' +
                ", code=" + code +
                ", errorMsg='" + errorMsg + '\'' +
                '}';
    }
}
