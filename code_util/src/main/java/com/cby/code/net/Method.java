package com.cby.code.net;

/**
 * @description: 请求方式
 * @author: ChenBaoYang
 * @createDate: 2022/1/18 5:28 下午
 */
public interface Method {

    int GET = 0;
    int POST = 1;
    int PUT = 2;
    int DELETE = 3;
    int HEAD = 4;
    int OPTIONS = 5;
    int TRACE = 6;

}
