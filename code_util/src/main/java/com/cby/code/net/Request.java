package com.cby.code.net;


import com.cby.code.net.policy.RetryPolicy;

/**
 * @description: Request
 * @author: ChenBaoYang
 * @createDate: 2022/1/18 9:57 下午
 */
public class Request {

    /** 请求地址 */
    private String url;

    /** {@linkplain com.cby.code.net.Method} */
    private int method;

    /** 文件路径（上传或者下载）*/
    private String filePath;

    /** 地址路径 */
    private String pathSegment = "";

    /** 请求参数 */
    private RequestParam param;

    /** 重试策略 */
    private RetryPolicy retryPolicy;

    protected Request(Builder builder) {
        this.url = builder.url;
        this.method = builder.method;
        this.filePath = builder.filePath;
        this.pathSegment = builder.pathSegment;
        this.param = builder.param;
        this.retryPolicy = builder.retryPolicy;
    }

    public String getUrl() {
        return url;
    }

    public int getMethod() {
        return method;
    }

    public String getPathSegment() {
        return pathSegment;
    }

    public RequestParam getParam() {
        return param;
    }

    public RetryPolicy getRetryPolicy() {
        return retryPolicy;
    }

    public String getFilePath() {
        return filePath;
    }

    public static class Builder{

        /** 请求地址 */
        private String url;

        /** {@linkplain com.cby.code.net.Method} */
        private int method;

        /** 文件路径（上传或者下载）*/
        private String filePath;

        /** 地址路径 */
        private String pathSegment;

        /** 请求参数 */
        private RequestParam param;

        /** 重试策略 */
        private RetryPolicy retryPolicy;

        public Builder setUrl(String url) {
            this.url = url;
            return this;
        }

        public Builder setMethod(int method) {
            this.method = method;
            return this;
        }

        public Builder setFilePath(String filePath) {
            this.filePath = filePath;
            return this;
        }

        public Builder setPathSegment(String pathSegment) {
            this.pathSegment = pathSegment;
            return this;
        }

        public Builder setRequestParam(RequestParam param) {
            this.param = param;
            return this;
        }

        public Builder setRetryPolicy(RetryPolicy retryPolicy) {
            this.retryPolicy = retryPolicy;
            return this;
        }

        public Request build(){
            return new Request(this);
        }

    }
}
