package com.cby.code.net2;

import com.cby.code.net.Response;
import com.cby.code.net.listener.ResponseListener;

import java.io.IOException;

/**
 * @description: NetCall
 * @author: ChenBaoYang
 * @createDate: 2022/2/28 9:32 上午
 */
public interface NetCall {

    Response apply(NetCallRequest callRequest);

    void apply(NetCallRequest callRequest, ResponseListener listener);

    Response downloadFile(NetCallRequest callRequest);

    void downloadFile(NetCallRequest callRequest, ResponseListener listener);


    Response uploadFile(NetCallRequest callRequest);

    void uploadFile(NetCallRequest callRequest, ResponseListener listener);
}
