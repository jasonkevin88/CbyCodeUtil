package com.cby.code.net2.action;

import com.cby.code.net.Request;
import com.cby.code.net.Response;
import com.cby.code.net.task.BaseTask;
import com.cby.code.net.task.HttpUrlStringTask;

/**
 * @description: 真正的网络请求执行者
 * @author: ChenBaoYang
 * @createDate: 2022/3/8 2:17 下午
 */
public class RealAction implements BaseAction{

    final BaseTask task;

    public RealAction() {
        task = new HttpUrlStringTask();
    }

    public RealAction(BaseTask task) {
        this.task = task;
    }

    @Override
    public Response doAction(Request request) {
        return task.performRequest(request);
    }
}
