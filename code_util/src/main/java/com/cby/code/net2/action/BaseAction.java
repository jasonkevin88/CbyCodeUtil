package com.cby.code.net2.action;

import com.cby.code.net.Request;
import com.cby.code.net.Response;

/**
 * @description: BaseAction
 * @author: ChenBaoYang
 * @createDate: 2022/3/8 2:15 下午
 */
public interface BaseAction {

    /**
     * 做请求
     * @param request
     * @return
     */
    Response doAction(Request request);
}
