package com.cby.code.net2;

import com.cby.code.net.Request;
import com.cby.code.net.RequestParam;
import com.cby.code.net.policy.RetryPolicy;

import java.util.Map;

/**
 * @description: NetCallRequestParam
 * @author: ChenBaoYang
 * @createDate: 2022/3/8 2:23 下午
 */
public class NetCallRequest {

    /** {@linkplain com.cby.code.net.Method} */
    private int method;

    /** 请求体 */
    private Request request;

    /** 请求参数 */
    private RequestParam param;

    /** 地址路径 */
    private String pathSegment;

    /** 文件路径（上传或者下载）*/
    private String filePath;

    protected NetCallRequest(Builder builder) {

        this.method = builder.method;
        this.pathSegment = builder.pathSegment;
        this.param = builder.paramBuilder.build();
        this.filePath = builder.filePath;
        this.request = builder.requestBuilder
                .setMethod(method)
                .setFilePath(builder.filePath)
                .setPathSegment(pathSegment)
                .setRequestParam(param)
                .setRetryPolicy(builder.retryPolicy)
                .build();
    }

    public Request getRequest() {
        return request;
    }

    public static final class Builder{

        /** {@linkplain com.cby.code.net.Method} */
        private int method;

        /** 文件路径（上传或者下载）*/
        private String filePath;

        /** 地址路径 */
        private String pathSegment;

        /** 重试策略 */
        private RetryPolicy retryPolicy;

        /** 请求构建对象 */
        private final Request.Builder requestBuilder = new Request.Builder();

        /** 请求参数构建对象 */
        private final RequestParam.Builder paramBuilder = new RequestParam.Builder();


        public NetCallRequest.Builder setMethod(int method) {
            this.method = method;
            return this;
        }

        public NetCallRequest.Builder setFilePath(String filePath) {
            this.filePath = filePath;
            return this;
        }

        public NetCallRequest.Builder setPathSegment(String pathSegment) {
            this.pathSegment = pathSegment;
            return this;
        }


        public NetCallRequest.Builder setRetryPolicy(RetryPolicy policy) {
            requestBuilder.setRetryPolicy(policy);
            return this;
        }

        public NetCallRequest.Builder addParam(String key, Object value) {
            paramBuilder.addParam(key, value);
            return this;
        }

        public NetCallRequest.Builder addAllParam(Map<String, Object> paramMap) {
            if(paramMap != null) {
                for(Map.Entry<String, Object> param: paramMap.entrySet()) {
                    paramBuilder.addParam(param.getKey(), param.getValue());
                }
            }
            return this;
        }

        public NetCallRequest.Builder addHeader(String key, String value) {
            paramBuilder.addHeader(key, value);
            return this;
        }

        public NetCallRequest.Builder addAllHeader(Map<String, String> headerMap) {
            if(headerMap != null) {
                for (Map.Entry<String, String> header: headerMap.entrySet()) {
                    paramBuilder.addHeader(header.getKey(), header.getValue());
                }
            }
            return this;
        }



        public NetCallRequest build(){
            return new NetCallRequest(this);
        }

    }
}
