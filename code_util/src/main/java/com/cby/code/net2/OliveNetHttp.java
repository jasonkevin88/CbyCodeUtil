package com.cby.code.net2;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @description: Olive
 * @author: ChenBaoYang
 * @createDate: 2022/3/8 11:38 上午
 */
public class OliveNetHttp {

    private OliveNetHttp() {}

    private static final Map<String, NetCall> netCallMap = new ConcurrentHashMap<>();

    public static NetCall createNetCall(String baseUrl) {
        NetCall call = netCallMap.get(baseUrl);
        if(call == null) {
            call = new OliveNetCallImpl(baseUrl);
            netCallMap.put(baseUrl, call);
        }
        return call;
    }

}
