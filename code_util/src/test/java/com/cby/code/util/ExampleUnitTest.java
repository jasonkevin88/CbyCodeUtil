package com.cby.code.util;

import org.junit.Test;

import static org.junit.Assert.*;

import com.cby.code.encrypt.Md5Utils;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }


    @Test
    public void testPcLogin() {
        //username=2191782708&time=1641275556204&fcm=0&uid=3289796968&token=a2bf763cfee19e7ba6d55f50bc112956
        String loginSecret = "f4e2de1af3e07820e6a701d3613cf5be";
        String uid = "3289796968";
        String userName = "2191782708";
        String time = "1641275556204";
        String token = Md5Utils.md5(Md5Utils.md5(uid+userName+loginSecret+time)+loginSecret);
        System.out.println("生成的登录密钥 = "+token);
        System.out.println("a2bf763cfee19e7ba6d55f50bc112956".equals(token));
        //$token = md5(md5($uid.$username.$loginSecret.$time).loginSecret);
    }
}