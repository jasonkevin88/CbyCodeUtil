package com.cby.util.sample;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.cby.code.helper.thread.Dispatcher;
import com.cby.code.helper.thread.ThreadHelper;
import com.cby.code.helper.thread.WorkTask;
import com.cby.code.optimization.UIFrameDetector;
import com.cby.code.util.AppUtils;
import com.cby.code.util.CbyLogUtils;
import com.cby.code.util.HandlerUtils;
import com.cby.code.util.SystemUtils;
import com.cby.code.util.ToastUtils;

/**
 * @author chenbaoyang
 */
public class MainActivity extends AppCompatActivity {

    private RadioGroup workRg;
    private RadioGroup finishRg;
    private TextView resultTv;

    private ThreadHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String appName = AppUtils.getAppName(MainActivity.this);
        ToastUtils.showToast(MainActivity.this, "测试自己远程库调用, app name = "+appName);
        CbyLogUtils.flog("测试CbyCodeUtil:1.0.3版本");
        helper = new ThreadHelper();
        workRg = findViewById(R.id.rg_work);
        finishRg = findViewById(R.id.rg_finish);
        resultTv = findViewById(R.id.tv_result);
        helper.workIn(Dispatcher.getMainThread());
        helper.endIn(Dispatcher.getMainThread());
        workRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio_main_thread_work:
                        helper.workIn(Dispatcher.getMainThread());
                        break;
                    case R.id.radio_current_thread_work:
                        helper.workIn(Dispatcher.getDefault());
                        break;
                    case R.id.radio_new_thread_work:
                        helper.workIn(Dispatcher.getNewThread());
                        break;
                }
            }
        });

        finishRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio_main_thread_finish:
                        helper.endIn(Dispatcher.getMainThread());
                        break;
                    case R.id.radio_current_thread_finish:
                        helper.endIn(Dispatcher.getDefault());
                        break;
                    case R.id.radio_new_thread_finish:
                        helper.endIn(Dispatcher.getNewThread());
                        break;
                }
            }
        });
    }



    public void onExecute(View view) {
        helper.handleWork(new WorkTask() {
            @Override
            public void doWork() {
                String log = "doWork-Thread = "+Thread.currentThread().getId() +"\n";
                HandlerUtils.runOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        resultTv.setText("");
                        resultTv.append(log);
                    }
                });

            }

            @Override
            public void onFinish() {
                String log = "onFinish-Thread = "+Thread.currentThread().getId();
                HandlerUtils.runOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        resultTv.append(log);
                    }
                });
            }
        });
    }

    @Override
    protected void onDestroy() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            UIFrameDetector.getInstance().stop();
        }
        super.onDestroy();
    }


    public void onStartUiFrameCheck(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            UIFrameDetector.getInstance().start();
        }
    }

    public void onStopUiFrameCheck(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            UIFrameDetector.getInstance().stop();
        }
    }

    public void openNetTestPage(View view) {
        Intent intent = new Intent(this, NetTestActivity.class);
        startActivity(intent);
    }

    public void cpuUsage(View view) {
        double result = SystemUtils.getProcessCpuUsage(android.os.Process.myPid()+"");
        resultTv.setText(result+"");
    }
}