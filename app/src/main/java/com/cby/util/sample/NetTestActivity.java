package com.cby.util.sample;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.cby.code.net.Method;
import com.cby.code.net.NetBrickPorter;
import com.cby.code.net.Response;
import com.cby.code.net.listener.ResponseListener;
import com.cby.code.net.policy.RetryPolicy;
import com.cby.code.net2.NetCall;
import com.cby.code.net2.NetCallRequest;
import com.cby.code.net2.OliveNetHttp;
import com.cby.code.util.HandlerUtils;
import com.cby.code.util.ThreadPoolUtil;

/**
 * @description: NetTestActivity
 * @author: ChenBaoYang
 * @createDate: 2022/3/3 3:27 下午
 */
public class NetTestActivity extends AppCompatActivity {

    private TextView showTv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_net_test);
        showTv = findViewById(R.id.tv_show);
    }

    public void requestNet(View view) {
        showTv.setText("loading");
        String url = "https://api.h5wan.4399sj.com/v20/app/startup/data.json";
        NetBrickPorter.getDefault().setBaseUrl(url)
                .setMethod(Method.GET)
                .setResponseListener(new ResponseListener() {
                    @Override
                    public void onResponse(Response response) {
                        try {
                            Log.i("chenby", response.toString());
                            handleResult(response.getData());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                })
                .setRetryPolicy(new RetryPolicy() {
                    @Override
                    public int getCurrentTimeout() {
                        return 7000;
                    }

                    @Override
                    public int getCurrentRetryCount() {
                        return 1;
                    }

                    @Override
                    public void retry(Exception error)  {
                        Log.e("chenby", error.getMessage());
                    }
                }).start();
    }

    private void handleResult(final String response) {
        HandlerUtils.runOnMainThread(new Runnable() {
            @Override
            public void run() {
                showTv.setText(response);
            }
        });
    }

    private static final String BASE_URL = "https://api.h5wan.4399sj.com/";
    private static final String PATH_SEGMENT = "v20/app/startup/data.json";

    NetCall call = OliveNetHttp.createNetCall(BASE_URL);

    public void requestNet2NotCallback(View view) {
        showTv.setText("loading");
        NetCallRequest request = new NetCallRequest.Builder()
                .setMethod(Method.GET)
                .setPathSegment(PATH_SEGMENT)
                .build();

        ThreadPoolUtil.execute(new Runnable() {
            @Override
            public void run() {
                Response response = call.apply(request);
                handleResult(response.getData());
            }
        });
    }

    public void requestNet2WithCallback(View view) {
        showTv.setText("loading");
        NetCallRequest request = new NetCallRequest.Builder()
                .setMethod(Method.GET)
                .setPathSegment(PATH_SEGMENT)
                .build();

        call.apply(request, new ResponseListener() {
            @Override
            public void onResponse(Response response) {
                handleResult(response.getData());
            }
        });

    }

    NetCall cdnCall = OliveNetHttp.createNetCall("https://cdn.h5wan.4399sj.com/");
    //https://cdn.h5wan.4399sj.com/public/images/20220217/38221345_86024900.jpg
    public void requestNet2DownLoadFile(View view) {
        String fileName = this.getExternalFilesDir("files").getAbsolutePath()+System.currentTimeMillis()+".jpg";
        showTv.setText("loading");
        NetCallRequest request = new NetCallRequest.Builder()
                .setMethod(Method.GET)
                .setPathSegment("public/images/20220217/38221345_86024900.jpg")
                .setFilePath(fileName)
                .build();

        cdnCall.downloadFile(request, new ResponseListener() {
            @Override
            public void onResponse(Response response) {
                handleResult(response.getData());
            }
        });
    }


    NetCall call2 = OliveNetHttp.createNetCall("https://cbycode.usemock.com/");
    //https://cbycode.usemock.com/file/upload
    public void requestNet2UploadFile(View view) {
        String fileName = this.getExternalFilesDir("files").getAbsolutePath()+"files1646728846057.jpg";
        showTv.setText("loading");
        NetCallRequest request = new NetCallRequest.Builder()
                .setMethod(Method.POST)
                .setPathSegment("file/upload")
                .setFilePath(fileName)
                .build();

        call2.uploadFile(request, new ResponseListener() {
            @Override
            public void onResponse(Response response) {
                handleResult(response.getData());
            }
        });
    }
}
